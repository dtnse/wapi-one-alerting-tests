@regression @v2endpoint
Feature: One alerting API

  Background:
    Given MG Authentication Token is set
#    Given Identity Authentication Token is set

  @WAPI-6094
  Scenario Outline: Verify v2 alerts are returned when required country query_param is provided
    Given that v2 API is queried with <showTotal> <country> and <excludeGeometry>
    When v2 alerts Get API request is sent using mg token
    Then the response status code is "200"
    And alerts are returned

    Examples: valid cases
      | country | showTotal | excludeGeometry |
      | US      | true      | true            |
      | us      | true      | true            |
      | DE      | true      | true            |
      | de      | true      | true            |

  @WAPI-6095
  Scenario Outline: Verify v2 error message when required country is not provided
    Given no <country> is provided to query the v2 API
    When v2 alerts Get API request is sent using mg token
    Then the response status code is "400"
    And correct required <country error response> is returned

    Examples: invalid country query param
      | country | country error response                             |
      | empty   | country is a required parameter ['US', 'DE', etc.] |

  @WAPI-6096
  Scenario Outline: Verify v2 alerts are returned when optional language is used
    Given language <language> and country <country> are provided to query the v2 API
    When v2 alerts Get API request is sent using mg token
    Then the response status code is "200"
    And alerts are returned

    Examples: valid cases
      | country | language |
      | US      | en-US    |
      | DE      | de-DE    |

  @WAPI-6145
  Scenario Outline: Verify v2 alerts are returned when optional event is used
    Given event <event> and country <country> are provided to query the v2 API
    When v2 alerts Get API request is sent using mg token
    Then the response status code is "200"
    And alerts are returned with <event> provided

    Examples: valid cases
      | country | event                |
      | US      | Small Craft Advisory |
      | US      | Flood Warning        |
      | DE      | light snowfall       |

  @WAPI-6097
  Scenario Outline: Verify v2 alerts are returned when optional sender parameter is used
    Given sender <sender> and country <country> are provided to query the v2 API
    When v2 alerts Get API request is sent using mg token
    Then the response status code is "200"
    And alerts are returned

    Examples: valid cases
      | country | sender |
      | US      | NWS    |
      | DE      | DWD    |

  @WAPI-6102
  Scenario Outline: Verify correct v2 alerts are returned based on startTime and endTime parameters
    Given startTime <startTime> endTime <endTime> and <country> are provided to query the v2 API
    When v2 alerts Get API request is sent using mg token
    Then the response status code is "200"
    And correct alerts are returned based on <startTime> and <endTime> parameters

    Examples: valid cases
      | country | startTime | endTime |
      | US      | valid     | empty   |
      | US      | empty     | valid   |
      | US      | valid     | valid   |


  @WAPI-6103
  Scenario Outline: Verify v2 error response when using invalid startTime and endTime parameters
    Given invalid startTime and endTime condition is provided with <country>
    When v2 alerts Get API request is sent using mg token
    Then the response status code is "400"
    And correct startTime and endTime error response is returned

    Examples: invalid time filters
      | country |
      | US      |
      | DE      |

  @WAPI-6106
  Scenario: Verify v2 error response when using empty startTime and endTime parameters
    Given empty startTime and endTime are provided
    When v2 alerts Get API request is sent using mg token
    Then the response status code is "422"
    And correct empty startTime and endTime error response is returned


  @WAPI-5589
  Scenario Outline: Verify v2 endpoint returns correct data as compared to the NWS/DWD alert
    Given v2 endpoint is queried with <showTotal> <excludeGeometry> <country> and <sender>
    When v2 alerts Get API request is sent using mg token
    Then the response status code is "200"
    And correct <sender> v2 data are returned

    Examples:
      | country | sender | showTotal | excludeGeometry |
      | US      | NWS    | true      | true            |
#      | DE      | DWD    | true      | true            |

  @WAPI-5885
  Scenario Outline: Verify geometry values of v2 endpoint API response for alerts with coordinates from NWS
    Given startTime endTime and <country> are provided to query the v2 API
    When v2 alerts Get API request is sent using mg token
    Then the response status code is "200"
    And coordinates from NWS are mapped in ABA alert

    Examples: valid cases
      | country |
      | US      |
#      | DE      |

  @WAPI-5886
  Scenario Outline: Verify geometry values of v2 endpoint API response for alerts without coordinates from NWS
    Given startTime endTime and <country> are provided to query the v2 API
    When v2 alerts Get API request is sent using mg token
    Then the response status code is "200"
    And geometries of NWS affectedZones are mapped in ABA alert

    Examples: valid cases
      | country |
      | US      |


  @WAPI-5676
  Scenario Outline: Verify required and optional parameters of v2 endpoint for DWD
    Given <country> <language> <sender> <excludeGeometry> and <event> are provided to query the v2 API
    When v2 alerts Get API request is sent using mg token
    Then the response status code is "200"
    And all parameters are returned in the response

    Examples: valid cases
      | country | language | sender | excludeGeometry | event   |
      | DE      | de-DE    | DWD    | true            | "FROST" |


