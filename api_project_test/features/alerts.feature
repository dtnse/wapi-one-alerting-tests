@regression @v1endpoint
Feature: One alerting API

  Background:
    Given MG Authentication Token is set
    Given Identity Authentication Token is set

  @WAPI-4900
  Scenario: Verify that API health check is working
    Given The health check endpoint is queried
    When Alerts Get API request is sent using mg token
    Then The status code is "200"
    And The response is UP


  @WAPI-5484 @smoke1
  Scenario Outline: Verify if Identity token is working for one alerting api
    Given The v1 endpoint is queried with country <country_code> sender <sender> and fields <fields>
    When Alerts Get API request is sent using identity token
    Then The status code is "200"
    And Response contains requested field <fields>

    Examples: valid fields
      | country_code | fields   | sender |
      | US           | language | NWS    |


  @WAPI-4401
  Scenario Outline: Verify response is correct when supported fields are used
    Given The v1 endpoint is queried with country <country_code> sender <sender> and fields <fields>
    When Alerts Get API request is sent using mg token
    Then The status code is "200"
    And Response contains requested field <fields>

    Examples: valid fields
      | country_code | fields      | sender |
      | US           | language    | NWS    |
      | US           | urgency     | NWS    |
      | US           | severity    | NWS    |
      | US           | certainty   | NWS    |
      | US           | onset       | NWS    |
      | US           | expires     | NWS    |
      | US           | headline    | NWS    |
      | US           | description | NWS    |
      | US           | category    | NWS    |
      | US           | event       | NWS    |
      | US           | eventCode   | NWS    |
      | US           | effective   | NWS    |
      | US           | senderName  | NWS    |
      | US           | parameters  | NWS    |
      | US           | areaDesc    | NWS    |
      | US           | polygon     | NWS    |
      | US           | geocodes    | NWS    |


  @WAPI-4402
  Scenario Outline: Verify response is correct when supported language are used
    Given The v1 endpoint is queried with language <language> and <sender>
    When Alerts Get API request is sent using mg token
    Then The status code is "200"
    And Verify response contains correct language <language>

    Examples: valid fields
      | language | sender |
      | en-US    | NWS    |
      | de-DE    | NWS    |
      | fr-FR    | NWS    |
      | it-IT    | NWS    |
      | es-ES    | NWS    |
      | pt-PT    | NWS    |
      | nl-NL    | NWS    |
      | fi-FI    | NWS    |
      | no       | NWS    |
      | da-DK    | NWS    |


  @WAPI-4403
  Scenario Outline: Show total
    Given The v1 endpoint is queried with show total <show_total> and <country_code> and <sender>
    When Alerts Get API request is sent using mg token
    Then The status code is "200"
    And Verify total_alerts field is based on <show_total>

    Examples: invalid fields
      | show_total | country_code | sender |
      | true       | DE           | DTN    |
      | false      | DE           | DTN    |
      | true       | US           | NWS    |
      | false      | US           | NWS    |

  @WAPI-4404
  Scenario Outline: Verify response is correct when country is invalid
    Given The v1 endpoint is queried with country <country_code> sender <sender> and fields <fields>
    When Alerts Get API request is sent using mg token
    Then The status code is "200"

    Examples: valid fields
      | country_code | fields   | sender |
      | XX           | language | NWS    |

  @WAPI-4405
  Scenario Outline: Verify That Response Is Correct When located Within Is Used
    Given The v1 endpoint is queried with locatedWithin <locatedWithin>
    When Alerts Get API request is sent using mg token
    Then The status code is "200"

    Examples: locatedWithin
      | locatedWithin                  |
      | -121.80, 37.50, -121.66, 37.27 |


  @WAPI-4406
  Scenario Outline: Invalid Show total
    Given The v1 endpoint is queried with show total <show_total> and <country_code> and <sender>
    When Alerts Get API request is sent using mg token
    Then The status code is "422"
    And The response contains correct showTotal error message

    Examples: invalid fields
      | show_total | country_code | sender |
      | invalid    | DE           | DTN    |

  @WAPI-4407
  Scenario Outline: Verify that error response is correct when geocode is present and geocode scheme is missing
    Given Geocode <geocode> is present and geocode scheme is missing
    When Alerts Get API request is sent using mg token
    Then The status code is "400"
    And Geocode error message is returned

    Examples: geocode value
      | geocode |
      | 54576   |

  @WAPI-4408
  Scenario Outline: Verify that error response is correct when geocode scheme is present and geocode is missing
    Given Geocode <geocode_scheme> is present and geocode is missing
    When Alerts Get API request is sent using mg token
    Then The status code is "400"
    And Geocode scheme error message is returned

    Examples: geocode scheme value
      | geocode_scheme |
      | UWZ            |

#  with bug WAPI-5493
#  @WAPI-4409
#  Scenario: Verify that error response is correct when locatedAt locatedWithin and country are not provided
#    Given locatedAt locatedWithin and country are not provided
#    When Alerts Get API request is sent using mg token
#    Then The status code is "400"
#    And Error message for missing locatedAt locatedWithin country is returned

  @WAPI-4410
  Scenario: Verify that error response is correct when there are two LocatedAt
    Given The v1 endpoint is queried with two LocatedAt
    When Alerts Get API request is sent using mg token
    Then The status code is "400"
    And locatedAt error message is returned

  @WAPI-4411
  Scenario Outline: Verify that error response is correct when offset is invalid
    Given The v1 endpoint is queried with offset <offset>
    When Alerts Get API request is sent using mg token
    Then The response status code is "422"
    And offset error message is returned

      Examples: valid fields
      | offset |
      | xxx    |

  @WAPI-4412
  Scenario Outline: Verify that error response Is correct when LocatedAt and locatedWithin and country are present
    Given The API is sent with <locatedAt> <locatedWithin> and <country_code>
    When Alerts Get API request is sent using mg token
    Then The status code is "400"
    And locatedAt locatedWithin country and error message is returned

    Examples: location values
      | country_code | locatedAt   | locatedWithin           |
      | DE           | 13.42,52.49 | 13.43,52.48,13.42,52.49 |



  @WAPI-4553
  Scenario Outline: Verify that API will return NWS data
    Given The v1 endpoint is queried with country <country_code> and sender <sender>
    When Alerts Get API request is sent using mg token
    Then The status code is "200"
    And The response contains correct nws data

    Examples: valid fields
      | country_code | sender |
      | US           | NWS    |


  @WAPI-5218 @smoke1
  Scenario Outline: Verify if api will return correct uwz data
    Given The v1 endpoint is queried with country <country_code> and sender <sender>
    When Alerts Get API request is sent using mg token
    Then The status code is "200"
    And The response contains correct uwz data

    Examples: valid fields
      | country_code | sender |
      | DE           | DTN    |


  @WAPI-5185
  Scenario Outline: Verify sender is correct when valid country is used
    Given The v1 endpoint is queried with show total <show_total> and <country_code> and <sender>
    When Alerts Get API request is sent using mg token
    Then The status code is "200"
    And Response contains the correct sender when country is <country_code>

    Examples: country and sender
      | show_total | country_code | sender |
      | true       | US           | NWS    |
      | true       | DE           | DTN    |


  @WAPI-5190
  Scenario Outline: Verify no alerts returned for invalid country and sender combination
    Given The v1 endpoint is queried with country <country_code> and sender <sender>
    When Alerts Get API request is sent using mg token
    Then The status code is "200"
    And No alerts are returned for each <country_code>

    Examples: invalid combination
      | country_code | sender |
      | US           | DTN    |
      | DE           | NWS    |

  @WAPI-5191
  Scenario Outline: Verify coordinates are mapped correctly for NWS alerts with polygon value
    Given The v1 endpoint is queried with country <country_code> and sender <sender>
    When Alerts Get API request is sent using mg token
    Then The status code is "200"
    And Coordinates are not empty for alerts with polygon values

    Examples: valid combination
      | country_code | sender |
      | US           | NWS    |

  @WAPI-5192
  Scenario Outline: Verify correct sender_name value for DE sender
    Given The v1 endpoint is queried with country <country_code> and sender <sender>
    When Alerts Get API request is sent using mg token
    Then The status code is "200"
    And The response contains sender_name value for <sender>

    # sender_name for DWD is yet to be implemented
    Examples: valid combination
      | country_code | sender |
      | DE           | DTN    |
#      | DE           | DWD    |

  @WAPI-5489
  Scenario Outline: Verify error messages for invalid fields filter
    Given The v1 endpoint is queried with country <country_code> sender <sender> and fields <fields>
    When Alerts Get API request is sent using mg token
    Then The status code is "400"
    And The response contains correct <fields> error message

    Examples: invalid fields
    | country_code | fields  | sender |
    | US           | empty   | NWS    |
    | US           | invalid | NWS    |

  @WAPI-5490 @smoke3
  Scenario Outline: Verify error messages for invalid query parameter
    Given The v1 endpoint is queried with <country_code> and invalid <query_parameter>
    When Alerts Get API request is sent using mg token
    Then The status code is "400"
    And The response contains correct query error message

    Examples: invalid query parameter
    | country_code | query_parameter  |
    | US           | field            |
    | US           | test             |