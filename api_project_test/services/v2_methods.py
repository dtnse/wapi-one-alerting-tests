from colorama import Fore
from core_utilities.api.utilities.common_api_utils import CommonApiUtils
from datetime import datetime, timezone, timedelta
import pytz
from pytz import timezone
import xmltodict, json

nws_alerts_url = 'https://api.weather.gov/alerts/urn:oid:'
dwd_alerts_url = 'https://www.dwd.de/DWD/warnungen/cap-feed/en/'


def to_utc_timestamp(timestamp):
    naive_updated_timestamp = datetime.strptime(timestamp, '%Y-%m-%dT%H:%M:%S%z')

    return naive_updated_timestamp.astimezone(timezone.utc).strftime('%Y-%m-%dT%H:%M:%S%z')


def set_time_param():
    utc = pytz.utc
    current_utc_time = datetime.now(utc)
    time_range = 1
    starttime = current_utc_time - timedelta(days=time_range)
    end_time = current_utc_time.strftime('%Y-%m-%dT%H:%M:%SZ')
    start_time = starttime.strftime('%Y-%m-%dT%H:%M:%SZ')
    return start_time, end_time


def set_invalid_time_param():
    utc = pytz.utc
    current_utc_time = datetime.now(utc)
    time_range = 2
    earlier_time = current_utc_time - timedelta(days=time_range)
    current_time = current_utc_time.strftime('%Y-%m-%dT%H:%M:%SZ')
    earlier_time = earlier_time.strftime('%Y-%m-%dT%H:%M:%SZ')
    return current_time, earlier_time


def verify_time_param(startTime, endTime, response):
    count = len(response['features'])
    counter = 0
    start_time, end_time = set_time_param()

    while counter < count:
        if startTime == "valid" and endTime == "empty":
            while counter < count:
                alerts_sentTime = response['features'][counter]['properties']['sent']
                assert start_time <= alerts_sentTime
                counter += 1

        elif startTime == "empty" and endTime == "valid":
            while counter < count:
                alerts_sentTime = response['features'][counter]['properties']['sent']
                assert end_time >= alerts_sentTime
                counter += 1

        else:
            while counter < count:
                alerts_sentTime = response['features'][counter]['properties']['sent']
                assert start_time <= alerts_sentTime, f"Failed: {startTime} is not less than {alerts_sentTime}"
                assert end_time >= alerts_sentTime
                counter += 1


def get_original_id(response, counter):
    param_list = response['features'][counter]['properties']['info'][0]['parameters']
    for parameters in param_list:
        param_dict = parameters
        if param_dict["param_key"] == "originalID":
            # print(param_dict["param_value"])
            original_id = param_dict["param_value"]
            return original_id


def access_nws_alert_url(response, counter):
    global nws_alerts_response
    nws_alert = nws_alerts_url + str(get_original_id(response, counter))
    nws_alerts_response = CommonApiUtils.common_request(nws_alert, method="Get", headers=None, body=None, json=None)
    # print(nws_alert)
    return nws_alerts_response


def access_dwd_alert_url(response, counter):
    dwd_alert = dwd_alerts_url + str(get_original_id(response, counter)) + ".xml"
    print("DWD Alert Url " + dwd_alert)
    dwd_alert_response = CommonApiUtils.common_request(dwd_alert, method="Get", headers=None, body=None, json=None)
    dwd_alert_content = dwd_alert_response.content

    # with open(
    #         "/test_data/dwd_alerts.xml",
    #         mode="w") as data:
    #     data.write(f"{dwd_alert_content}")

    return dwd_alert_content


def dwd_xml_to_dwd_json(dwd_alert_content):
    # Parse the XML string using the xmltodict library
    json_obj = xmltodict.parse(dwd_alert_content)

    #Serialize the JSON string into a Python object
    dwd_alert_content_json_data = json.dumps(json_obj)

    # print(dwd_alert_content_json_data)

    return dwd_alert_content_json_data


def assert_dwd_data(response, counter, dwd_json, sender):
    global aba_areaDesc_value, dwd_areaDesc_value
    aba_severity = response['features'][counter]['properties']['info'][0]['severity']
    dwd_severity = json.loads(dwd_json)
    dwd_severity_value = dwd_severity["alert"]["info"]["severity"]
    assert dwd_severity_value == aba_severity, Fore.RED + f"Failed: {dwd_severity_value} is not equal to {aba_severity}"

    aba_msgType = response['features'][counter]['properties']['msgType']
    dwd_msgType = json.loads(dwd_json)
    dwd_msgType_value = dwd_msgType["alert"]["msgType"]
    assert dwd_msgType_value == aba_msgType, Fore.RED + f"Failed: {dwd_msgType_value} is not equal to {aba_msgType}"

    aba_certainty = response['features'][counter]['properties']['info'][0]['certainty']
    dwd_certainty = json.loads(dwd_json)
    dwd_certainty_value = dwd_certainty["alert"]["info"]["certainty"]
    assert dwd_certainty_value == aba_certainty, Fore.RED + f"Failed: {dwd_certainty_value} is not equal to {aba_certainty}"

    aba_urgency = response['features'][counter]['properties']['info'][0]['urgency']
    dwd_urgency = json.loads(dwd_json)
    dwd_urgency_value = dwd_urgency["alert"]["info"]["urgency"]
    assert dwd_urgency_value == aba_urgency, Fore.RED + f"Failed: {dwd_urgency_value} is not equal to {aba_urgency}"

    aba_responseType = response['features'][counter]['properties']['info'][0]['responseType']
    dwd_responseType = json.loads(dwd_json)
    dwd_responseType_value = dwd_responseType["alert"]["info"]["responseType"]
    assert dwd_responseType_value == aba_responseType, Fore.RED + f"Failed: {dwd_responseType_value} is not equal to {aba_responseType}"

    aba_senderName = response['features'][counter]['properties']['info'][0]['senderName']
    dwd_senderName = json.loads(dwd_json)
    dwd_senderName_value = dwd_senderName["alert"]["info"]["senderName"]
    assert dwd_senderName_value == aba_senderName, Fore.RED + f"Failed: {dwd_senderName_value} is not equal to {aba_senderName}"

    aba_headline = response['features'][counter]['properties']['info'][0]['headline']
    dwd_headline = json.loads(dwd_json)
    dwd_headline_value = dwd_headline["alert"]["info"]["headline"]
    assert dwd_headline_value == aba_headline, Fore.RED + f"Failed: {dwd_headline_value} is not equal to {aba_headline}"

    aba_description = response['features'][counter]['properties']['info'][0]['description']
    dwd_description = json.loads(dwd_json)
    dwd_description_value = dwd_description["alert"]["info"]["description"]
    assert dwd_description_value == aba_description, Fore.RED + f"Failed: {dwd_description_value} is not equal to {aba_description}"

    aba_instruction = response['features'][counter]['properties']['info'][0]['instruction']
    dwd_instruction = json.loads(dwd_json)
    dwd_instruction_value = dwd_instruction["alert"]["info"]["instruction"]
    if dwd_instruction_value is None:
        assert aba_instruction == ""
    else:
        assert dwd_instruction_value in aba_instruction, Fore.RED + f"Failed: {dwd_instruction_value} is not equal to {aba_instruction}"

    aba_source = response['features'][counter]['properties']['source']
    dwd_source = json.loads(dwd_json)
    dwd_source_value = dwd_source["alert"]["sender"]
    assert dwd_source_value == aba_source, Fore.RED + f"Failed: {dwd_source_value} is not equal to {aba_source}"

    aba_areaDesc = response['features'][counter]['properties']['info'][0]['area']
    aba_areaDesc_Len = len(aba_areaDesc)
    counter_area = 0

    while counter_area < aba_areaDesc_Len:
        aba_areaDesc_value = response['features'][counter]['properties']['info'][0]['area'][counter_area]['areaDesc']
        dwd_areaDesc = json.loads(dwd_json)
        dwd_areaDesc_value = dwd_areaDesc["alert"]["info"]["area"][counter_area]["areaDesc"]

        assert dwd_areaDesc_value == aba_areaDesc_value, Fore.RED + f"Failed: {dwd_areaDesc_value} is not equal to {aba_areaDesc_value}. areadesc number is:  {counter_area}"

        counter_area += 1

    print(Fore.GREEN + f"Passed: Verify v2 endpoint returns correct data as compared to the {sender} alert")


def assert_nws_data(response, counter):
    aba_severity = response['features'][counter]['properties']['info'][0]['severity']
    nws_severity = nws_alerts_response.json()['properties']['severity']
    assert aba_severity == nws_severity

    aba_msgType = response['features'][counter]['properties']['msgType']
    nws_msgType = nws_alerts_response.json()['properties']['messageType']
    assert aba_msgType == nws_msgType

    aba_certainty = response['features'][counter]['properties']['info'][0]['certainty']
    nws_certainty = nws_alerts_response.json()['properties']['certainty']
    assert aba_certainty == nws_certainty

    aba_urgency = response['features'][counter]['properties']['info'][0]['urgency']
    nws_urgency = nws_alerts_response.json()['properties']['urgency']
    assert aba_urgency == nws_urgency

    aba_responseType = response['features'][counter]['properties']['info'][0]['responseType']
    nws_responseType = nws_alerts_response.json()['properties']['response']
    assert aba_responseType == nws_responseType

    aba_senderName = response['features'][counter]['properties']['info'][0]['senderName']
    nws_senderName = nws_alerts_response.json()['properties']['senderName']
    assert aba_senderName == nws_senderName

    aba_headline = response['features'][counter]['properties']['info'][0]['headline']
    nws_headline = nws_alerts_response.json()['properties']['headline']
    assert aba_headline in nws_headline

    aba_description = response['features'][counter]['properties']['info'][0]['description']
    nws_description = nws_alerts_response.json()['properties']['description']
    assert aba_description in nws_description

    aba_instruction = response['features'][counter]['properties']['info'][0]['instruction']
    nws_instruction = nws_alerts_response.json()['properties']['instruction']
    if nws_instruction is None:
        assert aba_instruction == ""
    else:
        assert aba_instruction in nws_instruction

    aba_source = response['features'][counter]['properties']['source']
    nws_sender = nws_alerts_response.json()['properties']['sender']
    assert aba_source == nws_sender

    aba_areaDesc = response['features'][counter]['properties']['info'][0]['area'][0]['areaDesc']
    nws_areaDesc = nws_alerts_response.json()['properties']['areaDesc']
    assert aba_areaDesc in nws_areaDesc



def assert_nws_aba_timestamp(response, counter):
    nws_sent = nws_alerts_response.json()['properties']['sent']
    to_utc_timestamp(nws_sent)
    aba_sent = response['features'][counter]['properties']['sent'] + "+0000"
    assert to_utc_timestamp(nws_sent) == aba_sent

    nws_effective = nws_alerts_response.json()['properties']['effective']
    nws_utc_effective = to_utc_timestamp(nws_effective)
    aba_effective = response['features'][counter]['properties']['info'][0]['effective'] + "+0000"
    assert nws_utc_effective == aba_effective

    nws_onset = nws_alerts_response.json()['properties']['onset']
    nws_utc_onset = to_utc_timestamp(nws_onset)
    aba_onset = response['features'][counter]['properties']['info'][0]['onset'] + "+0000"
    assert nws_utc_onset == aba_onset

    nws_expires = nws_alerts_response.json()['properties']['expires']
    nws_utc_expires = to_utc_timestamp(nws_expires)
    aba_expires = response['features'][counter]['properties']['info'][0]['expires'] + "+0000"
    assert nws_utc_expires == aba_expires


def check_nws_coordinates():
    nws_geometry = []
    if nws_alerts_response.json()['geometry'] is None:
        return nws_geometry
    else:
        nws_coordinates = nws_alerts_response.json()['geometry']['coordinates']
        return nws_coordinates


def access_nws_affected_zone_url(affected_zone_url):
    global nws_affected_zone_coordinates
    nws_affectedZone_response = CommonApiUtils.common_request(affected_zone_url, method="Get", headers=None,
                                                              body=None, json=None)
    nws_affected_zone_coordinates = nws_affectedZone_response.json()['geometry']['coordinates']
    print(nws_affected_zone_coordinates)
    return nws_affected_zone_coordinates


def append_affected_zones():
    nws_affected_zones = []
    affected_zone_list = nws_alerts_response.json()['properties']['affectedZones']
    for zone in affected_zone_list:
        affected_zone_url = zone
        affected_zone_coordinates = access_nws_affected_zone_url(affected_zone_url)
        nws_affected_zones.extend(affected_zone_coordinates)
        continue
    return nws_affected_zones


def verify_optional_dwd_parameters(response, counter):
    sender = response['features'][counter]['properties']['sender']
    assert "DWD" == sender, "Failed: Sender is not equal to DWD"
    language = response['features'][counter]['properties']['info'][counter]['language']
    assert "de-DE" == language, "Failed: Language is not equal to de-DE"
    event = response['features'][counter]['properties']['info'][counter]['event']
    assert event != ""