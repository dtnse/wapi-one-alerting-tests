from core_utilities.api.utilities.common_api_utils import CommonApiUtils
from datetime import datetime, timezone, timedelta, date

nws_alerts_url = 'https://api.weather.gov/alerts/urn:oid:'


def set_current_midnight():
    current_date = date.today()
    midnight = datetime.combine(current_date, datetime.min.time())
    current_midnight = midnight.strftime('%Y-%m-%dT%H:%M:%SZ')
    return current_midnight


def to_utc_timestamp(timestamp):
    naive_updated_timestamp = datetime.strptime(timestamp, '%Y-%m-%dT%H:%M:%S%z')
    return naive_updated_timestamp.astimezone(timezone.utc).strftime('%Y-%m-%dT%H:%M:%S%z')


def get_v1_original_id(response, counter):
    param_list = response['alerts'][counter]['info'][0]['parameters']
    for parameters in param_list:
        param_dict = parameters
        if param_dict["param_key"] == "originalID":
            # print(param_dict["param_value"])
            original_id = param_dict["param_value"]
            return original_id


def access_nws_alert_url(response, counter):
    global nws_alerts_response
    nws_alert = nws_alerts_url + str(get_v1_original_id(response, counter))
    nws_alerts_response = CommonApiUtils.common_request(nws_alert, method="Get", headers=None, body=None, json=None)
    # print(nws_alert)
    return nws_alerts_response


def assert_v1_nws_data(response, counter):
    aba_severity = response['alerts'][counter]['info'][0]['severity']
    nws_severity = nws_alerts_response.json()['properties']['severity']
    assert aba_severity == nws_severity

    aba_certainty = response['alerts'][counter]['info'][0]['certainty']
    nws_certainty = nws_alerts_response.json()['properties']['certainty']
    assert aba_certainty == nws_certainty

    aba_urgency = response['alerts'][counter]['info'][0]['urgency']
    nws_urgency = nws_alerts_response.json()['properties']['urgency']
    assert aba_urgency == nws_urgency

    aba_senderName = response['alerts'][counter]['info'][0]['senderName']
    nws_senderName = nws_alerts_response.json()['properties']['senderName']
    assert aba_senderName == nws_senderName

    aba_headline = response['alerts'][counter]['info'][0]['headline']
    nws_headline = nws_alerts_response.json()['properties']['headline']
    assert aba_headline in nws_headline

    aba_description = response['alerts'][counter]['info'][0]['description']
    nws_description = nws_alerts_response.json()['properties']['description']
    assert aba_description in nws_description

    aba_source = response['alerts'][counter]['source']
    nws_sender = nws_alerts_response.json()['properties']['sender']
    assert aba_source == nws_sender

    aba_areaDesc = response['alerts'][counter]['info'][0]['area']['areaDesc']
    nws_areaDesc = nws_alerts_response.json()['properties']['areaDesc']
    assert aba_areaDesc in nws_areaDesc

    aba_instruction = response['alerts'][counter]['info'][0]['instruction']
    nws_instruction = nws_alerts_response.json()['properties']['instruction']
    if nws_instruction is None:
        assert aba_instruction == ""
    else:
        assert aba_instruction in nws_instruction


def assert_v1_nws_aba_timestamp(response, counter):
    nws_sent = nws_alerts_response.json()['properties']['sent']
    to_utc_timestamp(nws_sent)
    aba_sent = response['alerts'][counter]['sent'] + "+0000"
    assert to_utc_timestamp(nws_sent) == aba_sent

    nws_effective = nws_alerts_response.json()['properties']['effective']
    nws_utc_effective = to_utc_timestamp(nws_effective)
    aba_effective = response['alerts'][counter]['info'][0]['effective'] + "+0000"
    assert nws_utc_effective == aba_effective

    nws_onset = nws_alerts_response.json()['properties']['onset']
    nws_utc_onset = to_utc_timestamp(nws_onset)
    aba_onset = response['alerts'][counter]['info'][0]['onset'] + "+0000"
    assert nws_utc_onset == aba_onset

    nws_expires = nws_alerts_response.json()['properties']['expires']
    nws_utc_expires = to_utc_timestamp(nws_expires)
    aba_expires = response['alerts'][counter]['info'][0]['expires'] + "+0000"
    assert nws_utc_expires == aba_expires

