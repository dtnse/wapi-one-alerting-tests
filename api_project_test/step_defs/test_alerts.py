import json
import time

import requests
from pytest_bdd import scenarios, given, when, then, parsers
from api_project_test.services.v1_methods import *
from api_project_test.config.configurations import get_config
from api_project_test.step_defs import test_data_warning_xml
from api_project_test.step_defs.test_data_detailed_warning_xml import detailed
from api_project_test.step_defs.test_data_warning_xml import warning

# import test_data_warning_xml
# from test_data_detailed_warning_xml import detailed
# from test_data_warning_xml import warning

config = get_config()

# Shared Variables
token_cred = config.get('creds', 'auth_cred')
mg_auth_token_url = config.get('urls', 'mg_auth_token_url')
identity_auth_token_url = config.get('urls', 'identity_auth_token_url')

warning_filename = config['filepath']['warning_filename']
detailed_filename = config['filepath']['detailed_filename']

# Scenarios
scenarios('../features/alerts.feature')


def identity_auth_token():
    headers = {'Content-Type': 'application/json', 'Accept': 'application/json'}
    payload = {"grant_type": "client_credentials", "client_id": "ntDhFcmpTVparAGaMqGezhg08vGJCh2d",
               "client_secret": "BSnoKhuR4jiZGizKPjuYMLtuRJ_4L54mpdkGYufgUJfV6NOXQgERaGLmF2SYaIKs",
               "audience": "https://one-alerting.dev.wx.zones.dtn.com/"}
    response = requests.post(identity_auth_token_url, headers=headers, data=None, json=payload).json()
    json_access_token = response['data']['access_token']
    return json_access_token


# Given Steps
@given("MG Authentication Token is set")
def mg_token():
    global mg_token
    mg_access_token = CommonApiUtils.auth_token(token_cred, mg_auth_token_url)
    mg_token = "Bearer " + mg_access_token


@given("Identity Authentication Token is set")
def identity_token():
    global identity_token
    identity_access_token = identity_auth_token()
    identity_token = "Bearer " + identity_access_token


@given(parsers.parse('The v1 endpoint is queried with country {country_code} sender {sender} and fields {fields}'),
       target_fixture='form_request')
def form_request(country_code, sender, fields):
    if fields == "empty":
        fields = ""

    api_request = config['urls']['v1_endpoint'] + "fields=" + fields + "&country=" + country_code + "&sender=" + sender

    return api_request


@given(parsers.parse('The health check endpoint is queried'), target_fixture='form_request')
def form_request():
    endpoint = config['urls']['health_check_endpoint']

    return endpoint


@given(parsers.parse('The v1 endpoint is queried with offset {offset}'), target_fixture='form_request')
def form_request(offset):
    endpoint = config['urls']['v1_endpoint'] + "country=US" + "&offset=" + offset

    return endpoint


@given(parsers.parse('The v1 endpoint is queried with language {language} and {sender}'), target_fixture='form_request')
def form_request(language, sender):
    api_request = config['urls']['v1_endpoint'] + "country=US" + "&language=" + language + "&sender=" + sender

    return api_request


@given(parsers.parse('The v1 endpoint is queried with show total {show_total} and {country_code} and {sender}'),
       target_fixture='form_request')
def form_request(show_total, country_code, sender):
    api_request = config['urls']['v1_endpoint'] + "showTotal=" + show_total + "&country=" + country_code + "&sender=" + sender

    return api_request


@given(parsers.parse('Geocode {geocode} is present and geocode scheme is missing'), target_fixture='form_request')
def form_request(geocode):
    endpoint = config['urls']['v1_endpoint'] + "country=US" + "&geocode=" + geocode

    return endpoint


@given(parsers.parse('Geocode {geocode_scheme} is present and geocode is missing'), target_fixture='form_request')
def form_request(geocode_scheme):
    endpoint = config['urls']['v1_endpoint'] + "country=US" + "&geocodeScheme=" + geocode_scheme

    return endpoint


@given('locatedAt locatedWithin and country are not provided', target_fixture='form_request')
def form_request():
    endpoint = config['urls']['v1_endpoint'] + "language=en-US&country=&fields=language"

    return endpoint


@given(parsers.parse('The v1 endpoint is queried with two LocatedAt'), target_fixture='form_request')
def form_request():
    endpoint = config['urls'][
                   'v1_endpoint'] + "showTotal=true&fields=language&locatedAt=-4.51513,35.38877&locatedAt=13.428103923797606,52.491181962857084"

    return endpoint


@given(parsers.parse('The API is sent with {locatedAt} {locatedWithin} and {country_code}'),
       target_fixture='form_request')
def form_request(locatedAt, locatedWithin, country_code):
    endpoint = config['urls'][
                   'v1_endpoint'] + "country=" + country_code + "&locatedAt=" + locatedAt + "&locatedWithin=" + locatedWithin

    return endpoint


@given(parsers.parse('The v1 endpoint is queried with locatedWithin {locatedWithin}'), target_fixture='form_request')
def form_request(locatedWithin):
    endpoint = config['urls']['v1_endpoint'] + "locatedWithin=" + locatedWithin

    return endpoint


@given(parsers.parse('The v1 endpoint is queried with country {country_code} and sender {sender}'),
       target_fixture='form_request')
def form_request(country_code, sender):
    api_request = config['urls']['v1_endpoint'] + "sender=" + sender + "&" + "country=" + country_code + set_current_midnight()

    return api_request


@given(parsers.parse('The v1 endpoint is queried with {country_code} and invalid {query_parameter}'),
       target_fixture='form_request')
def form_request(country_code, query_parameter):
    api_request = config['urls']['v1_endpoint'] + "country=" + country_code + "&" + query_parameter
    return api_request


# When Steps
@when(parsers.parse('Alerts {method} API request is sent using {token} token'), target_fixture='my_send_request')
def my_send_request(method, form_request, token):
    warning(warning_filename)
    detailed(detailed_filename)

    if token.lower() == 'identity':
        token = identity_token
    elif token.lower() == 'mg':
        token = mg_token
    else:
        token = mg_token

    headers = {'Authorization': token}
    request = config['urls']['dev_base_url'] + form_request
    response = CommonApiUtils.common_request(request, method, headers=headers, body=None, json=None)
    print(response.url)
    # print(response.content)

    return response


# Then Steps
@then(parsers.parse('The response contains correct {country_code} {zipcode} and {area_name}'))
def response_contents(my_send_request, country_code, zipcode, area_name):
    assert country_code == my_send_request.json()['_embedded']['areaMetadata']['countryCode']
    assert zipcode == my_send_request.json()['_embedded']['areaMetadata']['zipcode']
    assert area_name == my_send_request.json()['_embedded']['areaMetadata']['areaName']
    assert 'Polygon' == my_send_request.json()['_embedded']['areaMetadata']['polygon']['type']


@then(parsers.parse('The response status code is "{code:d}"'))
def ddg_response_code(my_send_request, code):
    assert my_send_request.status_code == code


@then(parsers.parse('The status code is "{code:d}"'))
def response_code(my_send_request, code):
    CommonApiUtils.verify_response_code(my_send_request, code)


@then(parsers.parse('The response contains correct nws data'))
def response_contents(my_send_request):
    response_json = my_send_request.content
    response = json.loads(response_json)
    count = len(response['alerts'])
    counter = 0

    while counter < count:
        # get aba alert original id
        get_v1_original_id(response, counter)

        # hit nws alerts url
        time.sleep(5)
        access_nws_alert_url(response, counter)

        # assert ABA v1 and nws data
        try:
            assert_v1_nws_data(response, counter)
            assert_v1_nws_aba_timestamp(response, counter)
        except Exception as e:
            print("Exception error encountered", e)

        # # assert ABA v1 and NWS timestamps
        # try:
        #     assert_v1_nws_aba_timestamp(response, counter)
        # except Exception as e:
        #     print("Exception error", e)

        counter += 1


@then(parsers.parse('The response contains correct uwz data'))
def response_contents(my_send_request):
    response_json = my_send_request.content
    response = json.loads(response_json)
    count = len(response['alerts'])
    counter = 0

    while counter < count:
        # alerts
        assert "uwz000" + test_data_warning_xml.warn_polyid == my_send_request.json()['alerts'][counter]['identifier']
        assert "DTN" == my_send_request.json()['alerts'][counter]['sender']
        assert "ACTUAL" == my_send_request.json()['alerts'][counter]['status']
        assert "Alert" == my_send_request.json()['alerts'][counter]['msgType']
        assert "Private" == my_send_request.json()['alerts'][counter]['scope']
        assert "dtn-forecaster" == my_send_request.json()['alerts'][0]['source']
        # alerts.info

        assert test_data_warning_xml.g_weather_type == my_send_request.json()['alerts'][counter]['info'][0]['event'][0]
        assert test_data_warning_xml.g_weather_type == \
               my_send_request.json()['alerts'][counter]['info'][0]['eventCode'][0]
        assert "MET" == my_send_request.json()['alerts'][counter]['info'][0]['category'][0]
        assert "Immediate" == my_send_request.json()['alerts'][counter]['info'][0]['urgency']
        assert "Moderate" == my_send_request.json()['alerts'][counter]['info'][0]['severity']
        assert "Possible" == my_send_request.json()['alerts'][counter]['info'][0]['certainty']
        assert "DTN" == my_send_request.json()['alerts'][counter]['info'][0]['senderName']
        assert test_data_warning_xml.st_lang_en == my_send_request.json()['alerts'][counter]['info'][0]['headline']
        assert test_data_warning_xml.lt_lang_en == my_send_request.json()['alerts'][counter]['info'][0]['description']

        # alerts.info.parameters
        assert "windspeed" == my_send_request.json()['alerts'][counter]['info'][0]['parameters'][0]['param_key']
        assert "75 km/h" == my_send_request.json()['alerts'][counter]['info'][0]['parameters'][0]['param_value']

        # alerts.info.area.geocodes
        assert {'value': 'DE38700', 'valueName': 'UWZ'} == \
               my_send_request.json()['alerts'][counter]['info'][0]['area']['geocodes'][0]
        assert {'value': 'DE37444', 'valueName': 'UWZ'} == \
               my_send_request.json()['alerts'][counter]['info'][0]['area']['geocodes'][1]
        assert {'value': 'DE38707', 'valueName': 'UWZ'} == \
               my_send_request.json()['alerts'][counter]['info'][0]['area']['geocodes'][2]
        assert {'value': 'DE38855', 'valueName': 'UWZ'} == \
               my_send_request.json()['alerts'][counter]['info'][0]['area']['geocodes'][3]
        assert {'value': 'DE38871', 'valueName': 'UWZ'} == \
               my_send_request.json()['alerts'][counter]['info'][0]['area']['geocodes'][4]
        counter = counter + 1


@then(parsers.parse('The response is UP'))
def response_contents(my_send_request):
    assert 'UP' == my_send_request.json()['status']


@then(parsers.parse('Verify response contains correct language {language}'))
def response_contents(my_send_request, language):
    response_json = my_send_request.content
    response = json.loads(response_json)
    count = len(response['alerts'])
    counter = 0

    while counter < count:
        assert language == my_send_request.json()['alerts'][counter]['info'][0]['language']
        counter = counter + 1


@then(parsers.parse('Verify total_alerts field is based on {show_total}'))
def response_contents(my_send_request, show_total):
    response_json = my_send_request.content
    response = json.loads(response_json)

    if show_total == 'true':
        assert 'total_alerts' in response
    elif show_total == 'false':
        assert 'total_alerts' not in response


@then(parsers.parse('Verify other default data is correct'))
def response_contents(my_send_request, language):
    response_json = my_send_request.content
    response = json.loads(response_json)
    count = len(response['alerts'])
    counter = 0

    while counter < count:
        assert "DTN" == my_send_request.json()['alerts'][counter]['sender']
        assert "ACTUAL" == my_send_request.json()['alerts'][counter]['status']
        assert "Alert" == my_send_request.json()['alerts'][counter]['msgType']
        assert "Private" == my_send_request.json()['alerts'][counter]['scope']
        assert "DTN" == my_send_request.json()['alerts'][counter]['info'][0]['senderName']
        counter = counter + 1


@then(parsers.parse('Response contains requested field {field}'))
def response_contents(my_send_request, field):
    response_json = my_send_request.content
    response = json.loads(response_json)
    count = len(response['alerts'])
    counter = 0

    area_fields_list = ['areaDesc', 'polygon', 'geocodes']

    while counter < count:
        if field in area_fields_list:
            assert "" != my_send_request.json()['alerts'][counter]['info'][0]['area'][field]

        else:
            assert "" != my_send_request.json()['alerts'][counter]['info'][0][field]

        counter = counter + 1


@then(parsers.parse('Response contains correct {source} when country is {country_code}'))
def response_contents(my_send_request, source, country_code):
    response_json = my_send_request.content
    response = json.loads(response_json)
    count = len(response['alerts'])
    counter = 0

    while counter < count:
        if country_code == 'US':
            assert source == my_send_request.json()['alerts'][counter]['source']
            assert "test" == my_send_request.json()['alerts'][counter]['info'][counter]['area']['polygon']['geometry'][
                'coordinates']

            # nws_alerts_json_response()

        elif country_code == 'DE':
            assert source == my_send_request.json()['alerts'][counter]['source']

        else:
            assert "nws" == my_send_request.json()['alerts'][counter]['source']

        counter = counter + 1


@then('Geocode error message is returned')
def response_contents(my_send_request):
    error_message = "geocode_scheme is required when using geocode parameter"
    assert error_message == my_send_request.json()['detail']


@then('Geocode scheme error message is returned')
def response_contents(my_send_request):
    error_message = "geocode is required when using geocode_scheme parameter"
    assert error_message == my_send_request.json()['detail']


@then('locatedAt error message is returned')
def response_contents(my_send_request):
    error_message = "Too many coordinate points for locatedAt. max: 2"
    assert error_message == my_send_request.json()['detail']


@then('offset error message is returned')
def response_contents(my_send_request):
    error_message = "value is not a valid integer"
    assert error_message == my_send_request.json()['detail'][0]['msg']


@then('locatedAt locatedWithin country and error message is returned')
def response_contents(my_send_request):
    error_message = "Only one of locatedAt, locatedWithin or country should be provided"
    assert error_message == my_send_request.json()['detail']


@then('Error message for missing locatedAt locatedWithin country is returned')
def response_contents(my_send_request):
    error_message = "One of locatedAt, locatedWithin or country needs to be provided"
    assert error_message == my_send_request.json()['detail']


@then('The response contains correct offset error message')
def response_contents(my_send_request):
    error_message = "value is not a valid integer"
    assert error_message == my_send_request.json()['detail'][0]['msg']


@then('The response contains correct showTotal error message')
def response_contents(my_send_request):
    error_message = "value could not be parsed to a boolean"
    assert error_message == my_send_request.json()['detail'][0]['msg']


@then(parsers.parse('Response contains the correct sender when country is {country_code}'))
def response_contents(my_send_request, country_code):
    response_json = my_send_request.content
    response = json.loads(response_json)
    count = len(response['alerts'])
    counter = 0

    while counter < count:
        if country_code == 'US':
            assert "NWS" == my_send_request.json()['alerts'][counter]['sender']

            # nws_alerts_json_response()

        elif country_code == 'DE':
            de_sender = ['DTN', 'DWD']
            response_sender = my_send_request.json()['alerts'][counter]['sender']
            assert response_sender in de_sender

        counter = counter + 1


@then(parsers.parse('No alerts are returned for each {country_code}'))
def response_contents(my_send_request, country_code):
    response_json = my_send_request.content
    response = json.loads(response_json)
    count = len(response['alerts'])
    if country_code == 'US':
        assert 0 == count

    elif country_code == 'DE':
        assert 0 == count


@then(parsers.parse('The response contains sender_name value for {sender}'))
def response_contents(my_send_request, sender):
    response_json = my_send_request.content
    response = json.loads(response_json)
    count = len(response['alerts'])
    counter = 0

    while counter < count:
        aba_v1_sendername = response['alerts'][counter]['info'][0]['senderName']
        if sender == 'DTN':
            assert 'DTN' == aba_v1_sendername

        counter = counter + 1


@then(parsers.parse('Coordinates are not empty for alerts with polygon values'))
def response_contents(my_send_request):
    response_json = my_send_request.content
    response = json.loads(response_json)
    count = len(response['alerts'])
    counter = 0

    while counter < count:
        aba_v1_coordinates = response['alerts'][counter]['info'][0]['area']['polygon']['geometry']['coordinates']

        # get aba alert original id
        get_v1_original_id(response, counter)

        # hit nws alerts url
        time.sleep(5)
        nws_alerts_response = access_nws_alert_url(response, counter)

        # check nws for geometry value
        try:
            nws_geometry = nws_alerts_response.json()['geometry']
        except Exception as e:
            print(nws_alerts_response, e)

            # assert aba coordinates value
            if nws_geometry is not None:
                assert [] != aba_v1_coordinates

            elif nws_geometry is None:
                assert [] == aba_v1_coordinates

        counter += 1


@then(parsers.parse("The response contains correct {fields} error message"))
def response_contents(my_send_request, fields):
    if fields == 'empty':
        fields_error_message = "Fields should contain at least one value when passed. Valid fields: ['language', 'category', 'event', 'urgency', 'severity', 'certainty', 'eventCode', 'effective', 'onset', 'expires', 'sender_name', 'senderName', 'headline', 'description', 'parameter', 'parameters', 'area_desc', 'areaDesc', 'polygon', 'geocode', 'geocodes']"
        assert fields_error_message == my_send_request.json()['detail']

    elif fields == 'invalid':
        fields_error_message = "Invalid field is passed. Valid fields: ['language', 'category', 'event', 'urgency', 'severity', 'certainty', 'eventCode', 'effective', 'onset', 'expires', 'sender_name', 'senderName', 'headline', 'description', 'parameter', 'parameters', 'area_desc', 'areaDesc', 'polygon', 'geocode', 'geocodes']"
        assert fields_error_message == my_send_request.json()['detail']


@then(parsers.parse('The response contains correct query error message'))
def response_contents(my_send_request):
    query_error_message = "Invalid query parameter is passed. Valid Query Parameters: ['fields', 'event', 'locatedAt', 'locatedWithin', 'country', 'geocodeScheme', 'geocode', 'effective', 'onset', 'updated', 'language', 'sender', 'country', 'showTotal', 'show_total', 'offset']"
    assert query_error_message == my_send_request.json()['detail']
