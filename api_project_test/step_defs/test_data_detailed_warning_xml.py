import xml.etree.ElementTree as xml
from datetime import date, timedelta

warn_polyid = "16366607974214"

today = date.today()
today_plus_days = date.today() + timedelta(days=1)


g_current_year = today.strftime("%Y")
g_current_month = today.strftime("%m")
g_current_day = today.strftime("%d")
g_expiration_day = today_plus_days.strftime("%d")
g_weather_type = "storm"
g_alt_min = "800"
g_alt_max = "9000"

# language
st_lang_de = "Im Harz oberhalb von 800 Metern Sturmboen von 60-80 km/h moglich."
st_lang_en = "Above 600 meters: stormy gusts. 60-80 km/h."
st_lang_fr = "Au-dessus de 800 metres: des rafales tempetueuses. 60-80 km/h."
st_lang_nl = "Boven 800 meter: zware windstoten. 60-80 km/h."
st_lang_lu = "Iwwer vun 800 Meter: Stuermsteiss. 60-80 km/h."
st_lang_dk = "Ovenfor 800 meter: vindstod med stormstyrke. 60-80 km/h."
st_lang_it = "Sopra gli 800 metri: raffiche tempestose. 60-80 km/h."
st_lang_es = "Por encima de 800 metros: rafagas de tormenta. 60-80 km/h."
st_lang_sv = "Over 800 meter: stormbyar. 60-80 km/h."
st_lang_pt = "Acima de 800 metros: borrascas violentas. 60-999 km/h."
st_lang_fi = "800 metrin:ylapuolella myrskynpuuskia. 60-999 km/h."
st_lang_no = "Ovenfor fra 800 meter: Stormkast. 60-80 km/h."

lt_lang_de = "Fur Lagen oberhalb von 800 Metern: Ab Samstagmorgen sind zeitweise Sturmboen moglich. Dabei kommt es zu Sturmboen zwischen 60 und 80 km/h, ortlich auch mehr. Der Wind weht aus Sudwest. Samstagvormittag lassen die Sturmboen bereits wieder nach"
lt_lang_en = "For test May 20 areas above 700 meters: From Saturday morning stormy gusts may occur temporarily. Accordingly, gale-force gusts of 60 to 80 km/h are expected, locally even more. Winds blow from southwest. Saturday forenoon stormy gusts decrease"
lt_lang_fr = "Pour des zones au-dessus de 800 metres: A partir de Samedi matin, des rafales tempetueuses sont possibles de temps en temps. Il y aura des rafales tempetueuses comprises entre 60 et 80 km/h, localement meme plus. Le vent souffle de secteur sud-ouest. Samedi dans la matinee, les rafales tempetueuses cessent."
lt_lang_nl = "Voor gebieden boven 800 meter: Vanaf zaterdag morgen zijn er tijdelijk zware windstoten mogelijk. Hierbij zijn er zware windstoten tussen 60 en 80 km/u, plaatselijk meer. De wind komt uit het zuidwesten. zaterdag voormiddag verminderen zware windstoten."
lt_lang_lu = "Fir Lagen iwwer vun 800 Meter: Vun Samschdeg Moien sinn zaitweis Stuermsteiss meiglech. Dobai kennt et zu Stuermsteiss teschent 60 an 80 km/h, lokal och mei. De Wand bleist aus Sudwest. Samschdeg Virmetteg loossen d'Stuermsteiss no."
lt_lang_dk = "For omrader ovenfor 800 meter: Fra Lordag morgen kan der forekomme lejlighedsvis vindstod med stormstyrke pa steder. Der kan forekomme vindstod af stormstyrke mellem 60 og 80 km/h, stedvist ogsa mere. Vinden kommer fra sydvest. Fra Lordag formiddag aftager vindstodene med stormstyrke i omrader."
lt_lang_it = "Per zone sopra gli 800 metri: Da Sabato mattina sono a tratti probabili raffiche tempestose. Le raffiche tempestose sono comprese fra 60 e 80 km/h, localmente anche oltre. Il vento proviene da sudovest. Sabato in mattinata le raffiche tempestose perdono di intensita."
lt_lang_es = "Para por encima de 800 metros: A partir de sabado por la manana temporalmente posibles rafagas de tormenta en. Ocurrenrafagas de tormenta entre 60 y 80 km/h, regional aun mas. El viento sopla del suroeste. sabado por la manana cesan los/las rafagas de tormenta en."
lt_lang_sv = "For platser over 800 meter: Fr.o.m. lordag morgon finns det tidvis mojlighet till stormbyar. Detta leder till stormbyar mellan 60 och 80 km/h, lokalt ocksa hogre. Vinden blaser fran sydvast. Pa lordag formiddag avtar stormbyarna."
lt_lang_pt = "Para locais acima de 800 metros: Ate Sabado ao inicio da manha, sao possiveis, ocasionalmente, borrascas violentas. Com borrascas violentas entre 60 e 80 km/h, localmente tambem com maior intensidade. O vento sopra de sudoeste. Sabado de manha, as borrascas violentas diminuem."
lt_lang_fi = "Asemille 800 metrin ylapuolella: Lauantaista lahtien aamulla on mahdollista saada ajoittain myrskynpuuskia. Talloin on odotettavissa myrskynpuuskia, joiden nopeudet vaihtelevat 60 - 80 km/h, paikallisesti myos enemman. Tuuli puhaltaa suunnasta lounas. Lauantaina aamupaivalla myrskynpuuskat vahenevat."
lt_lang_no = "For varsituasjoner ovenfor fra 800 meter: Fra Lordag Morgen er tidvis Stormkast mulig. Derved kann det komme Stormkast mellom 60 und 80 km/h, lokalt ogsa mer. Vinden blaser fra Sorvest. Lordag Formiddag avtar Stormkast."

# geocode
valid_geocode_1_DE38700 = "DE38700"

def detailed(fileName):
    root = xml.Element("warning")
    v_warn_polyid = xml.Element("warnpolyid")
    v_producer = xml.Element("producer")
    v_source = xml.Element("source")
    v_creation_time = xml.Element("creation-time")
    v_event_start_time = xml.Element("eventstart-time")
    v_event_end_time = xml.Element("eventend-time")
    v_weather_type = xml.Element("weathertype")
    v_warn_spec = xml.Element("warnspec")
    v_alt_min = xml.Element("altmin")
    v_alt_max = xml.Element("altmax")
    v_short_text = xml.Element("short-text")
    v_long_text = xml.Element("long-text")
    v_user_list = xml.Element("userlist")

    # append to root
    root.append(v_warn_polyid)
    root.append(v_producer)
    root.append(v_source)
    root.append(v_creation_time)
    root.append(v_event_start_time)
    root.append(v_event_end_time)
    root.append(v_weather_type)
    root.append(v_warn_spec)
    root.append(v_alt_min)
    root.append(v_alt_max)
    root.append(v_short_text)
    root.append(v_long_text)
    root.append(v_user_list)

    v_warn_polyid.text = warn_polyid
    v_producer.text = "fschwienbacher27"
    v_source.text = " "
    v_creation_time.text = " "
    v_event_start_time.text = " "
    v_weather_type.text = g_weather_type
    v_warn_spec.text = " "
    v_alt_min.text = g_alt_min
    v_alt_max.text = g_alt_max
    v_short_text.text = " "
    v_user_list.text = " "

    # creation time
    ct_year = g_current_year
    ct_month = g_current_month
    ct_day = g_current_day
    ct_hour = "00"
    ct_min = "00"
    ct_sec = "00"
    year = xml.SubElement(v_creation_time, "year")
    year.text = ct_year
    month = xml.SubElement(v_creation_time, "mon")
    month.text = ct_month
    day = xml.SubElement(v_creation_time, "day")
    day.text = ct_day
    hour = xml.SubElement(v_creation_time, "hour")
    hour.text = ct_hour
    min = xml.SubElement(v_creation_time, "min")
    min.text = ct_min
    sec = xml.SubElement(v_creation_time, "sec")
    sec.text = ct_sec


    # event start time
    est_year = g_current_year
    est_month = g_current_month
    est_day = g_current_day
    est_hour = "00"
    est_min = "00"
    est_sec = "00"
    year = xml.SubElement(v_event_start_time, "year")
    year.text = est_year
    month = xml.SubElement(v_event_start_time, "mon")
    month.text = est_month
    day = xml.SubElement(v_event_start_time, "day")
    day.text = est_day
    hour = xml.SubElement(v_event_start_time, "hour")
    hour.text = est_hour
    min = xml.SubElement(v_event_start_time, "min")
    min.text = est_min
    sec = xml.SubElement(v_event_start_time, "sec")
    sec.text = est_sec

    # event end time
    eet_year = g_current_year
    eet_month = g_current_month
    eet_day = g_expiration_day
    eet_hour = "00"
    eet_min = "00"
    eet_sec = "00"
    year = xml.SubElement(v_event_end_time, "year")
    year.text = eet_year
    month = xml.SubElement(v_event_end_time, "mon")
    month.text = eet_month
    day = xml.SubElement(v_event_end_time, "day")
    day.text = eet_day
    hour = xml.SubElement(v_event_end_time, "hour")
    hour.text = eet_hour
    min = xml.SubElement(v_event_end_time, "min")
    min.text = eet_min
    sec = xml.SubElement(v_event_end_time, "sec")
    sec.text = eet_sec

    # warnspec
    warnspec = xml.SubElement(v_warn_spec, "item")
    ws = xml.SubElement(warnspec, "subtype")
    ws.text = "windspeed"
    ws = xml.SubElement(warnspec, "number")
    ws.text = "75"
    ws = xml.SubElement(warnspec, "unit")
    ws.text = "km/h"

    # short text
    s_lang_de = xml.SubElement(v_short_text, "lang-de")
    s_lang_en = xml.SubElement(v_short_text, "lang-en")
    s_lang_fr = xml.SubElement(v_short_text, "lang-fr")
    s_lang_nl = xml.SubElement(v_short_text, "lang-nl")
    s_lang_lu = xml.SubElement(v_short_text, "lang-lu")
    s_lang_dk = xml.SubElement(v_short_text, "lang-dk")
    s_lang_it = xml.SubElement(v_short_text, "lang-it")
    s_lang_es = xml.SubElement(v_short_text, "lang-es")
    s_lang_sv = xml.SubElement(v_short_text, "lang-sv")
    s_lang_pt = xml.SubElement(v_short_text, "lang-pt")
    s_lang_fi = xml.SubElement(v_short_text, "lang-fi")
    s_lang_no = xml.SubElement(v_short_text, "lang-no")
    s_lang_de.text = st_lang_de
    s_lang_en.text = st_lang_en
    s_lang_fr.text = st_lang_fr
    s_lang_nl.text = st_lang_nl
    s_lang_lu.text = st_lang_lu
    s_lang_dk.text = st_lang_dk
    s_lang_it.text = st_lang_it
    s_lang_es.text = st_lang_es
    s_lang_sv.text = st_lang_sv
    s_lang_pt.text = st_lang_pt
    s_lang_fi.text = st_lang_fi
    s_lang_no.text = st_lang_no

    # long text
    l_lang_de = xml.SubElement(v_long_text, "lang-de")
    l_lang_de.text = lt_lang_de
    l_lang_en = xml.SubElement(v_long_text, "lang-en")
    l_lang_en.text = lt_lang_en
    l_lang_fr = xml.SubElement(v_long_text, "lang-fr")
    l_lang_fr.text = lt_lang_fr
    l_lang_nl = xml.SubElement(v_long_text, "lang-nl")
    l_lang_nl.text = lt_lang_nl
    l_lang_lu = xml.SubElement(v_long_text, "lang-lu")
    l_lang_lu.text = lt_lang_lu
    l_lang_dk = xml.SubElement(v_long_text, "lang-dk")
    l_lang_dk.text = lt_lang_dk
    l_lang_it = xml.SubElement(v_long_text, "lang-it")
    l_lang_it.text = lt_lang_it
    l_lang_es = xml.SubElement(v_long_text, "lang-es")
    l_lang_es.text = lt_lang_es
    l_lang_sv = xml.SubElement(v_long_text, "lang-sv")
    l_lang_sv.text = lt_lang_sv
    l_lang_pt = xml.SubElement(v_long_text, "lang-pt")
    l_lang_pt.text = lt_lang_pt
    l_lang_fi = xml.SubElement(v_long_text, "lang-fi")
    l_lang_fi.text = lt_lang_fi
    l_lang_no = xml.SubElement(v_long_text, "lang-no")
    l_lang_no.text = lt_lang_no

    # user list
    user_list = xml.SubElement(v_user_list, "user")
    user_list.text = " "
    user = xml.SubElement(user_list, "id")
    user.text = "UWZDL-WR"
    user = xml.SubElement(user_list, "level")
    user.text = "7"
    user = xml.SubElement(user_list, "web")
    user.text = "1"
    user = xml.SubElement(user_list, "email")
    user.text = "1"
    user = xml.SubElement(user_list, "uwzd")
    user.text = "1"
    user = xml.SubElement(user_list, "levelname")
    user.text = "alert_forewarn_orange"

    user_list = xml.SubElement(v_user_list, "user")
    user_list.text = " "
    user = xml.SubElement(user_list, "id")
    user.text = "UWZDL-OHA"
    user = xml.SubElement(user_list, "level")
    user.text = "7"
    user = xml.SubElement(user_list, "web")
    user.text = "1"
    user = xml.SubElement(user_list, "email")
    user.text = "1"
    user = xml.SubElement(user_list, "uwzd")
    user.text = "1"
    user = xml.SubElement(user_list, "levelname")
    user.text = "alert_forewarn_orange"

    user_list = xml.SubElement(v_user_list, "user")
    user_list.text = " "
    user = xml.SubElement(user_list, "id")
    user.text = "UWZTK00008"
    user = xml.SubElement(user_list, "level")
    user.text = "7"
    user = xml.SubElement(user_list, "web")
    user.text = "1"
    user = xml.SubElement(user_list, "email")
    user.text = "1"
    user = xml.SubElement(user_list, "uwzd")
    user.text = "1"
    user = xml.SubElement(user_list, "levelname")
    user.text = "alert_forewarn_orange"

    user_list = xml.SubElement(v_user_list, "user")
    user_list.text = " "
    user = xml.SubElement(user_list, "id")
    user.text = "UWZTK00016"
    user = xml.SubElement(user_list, "level")
    user.text = "7"
    user = xml.SubElement(user_list, "web")
    user.text = "1"
    user = xml.SubElement(user_list, "email")
    user.text = "1"
    user = xml.SubElement(user_list, "uwzd")
    user.text = "1"
    user = xml.SubElement(user_list, "levelname")
    user.text = "alert_forewarn_orange"

    user_list = xml.SubElement(v_user_list, "user")
    user_list.text = " "
    user = xml.SubElement(user_list, "id")
    user.text = "UWZTK00020"
    user = xml.SubElement(user_list, "level")
    user.text = "7"
    user = xml.SubElement(user_list, "web")
    user.text = "1"
    user = xml.SubElement(user_list, "email")
    user.text = "1"
    user = xml.SubElement(user_list, "uwzd")
    user.text = "1"
    user = xml.SubElement(user_list, "levelname")
    user.text = "alert_forewarn_orange"

    user_list = xml.SubElement(v_user_list, "user")
    user_list.text = " "
    user = xml.SubElement(user_list, "id")
    user.text = "UWZDL"
    user = xml.SubElement(user_list, "level")
    user.text = "7"
    user = xml.SubElement(user_list, "web")
    user.text = "1"
    user = xml.SubElement(user_list, "email")
    user.text = "1"
    user = xml.SubElement(user_list, "uwzd")
    user.text = "1"
    user = xml.SubElement(user_list, "levelname")
    user.text = "alert_forewarn_orange"

    user_list = xml.SubElement(v_user_list, "user")
    user_list.text = " "
    user = xml.SubElement(user_list, "id")
    user.text = "XXXXX38879"
    user = xml.SubElement(user_list, "level")
    user.text = "7"
    user = xml.SubElement(user_list, "web")
    user.text = "1"
    user = xml.SubElement(user_list, "email")
    user.text = "1"
    user = xml.SubElement(user_list, "uwzd")
    user.text = "1"
    user = xml.SubElement(user_list, "levelname")
    user.text = "alert_forewarn_orange"

    user_list = xml.SubElement(v_user_list, "user")
    user_list.text = " "
    user = xml.SubElement(user_list, "id")
    user.text = "UWZ" + valid_geocode_1_DE38700
    user = xml.SubElement(user_list, "level")
    user.text = "7"
    user = xml.SubElement(user_list, "web")
    user.text = "1"
    user = xml.SubElement(user_list, "email")
    user.text = "1"
    user = xml.SubElement(user_list, "uwzd")
    user.text = "1"
    user = xml.SubElement(user_list, "levelname")
    user.text = "alert_forewarn_orange"

    user_list = xml.SubElement(v_user_list, "user")
    user_list.text = " "
    user = xml.SubElement(user_list, "id")
    user.text = "UWZDE37444"
    user = xml.SubElement(user_list, "level")
    user.text = "7"
    user = xml.SubElement(user_list, "web")
    user.text = "1"
    user = xml.SubElement(user_list, "email")
    user.text = "1"
    user = xml.SubElement(user_list, "uwzd")
    user.text = "1"
    user = xml.SubElement(user_list, "levelname")
    user.text = "alert_forewarn_orange"

    user_list = xml.SubElement(v_user_list, "user")
    user_list.text = " "
    user = xml.SubElement(user_list, "id")
    user.text = "UWZDE38707"
    user = xml.SubElement(user_list, "level")
    user.text = "7"
    user = xml.SubElement(user_list, "web")
    user.text = "1"
    user = xml.SubElement(user_list, "email")
    user.text = "1"
    user = xml.SubElement(user_list, "uwzd")
    user.text = "1"
    user = xml.SubElement(user_list, "levelname")
    user.text = "alert_forewarn_orange"

    user_list = xml.SubElement(v_user_list, "user")
    user_list.text = " "
    user = xml.SubElement(user_list, "id")
    user.text = "UWZDE38855"
    user = xml.SubElement(user_list, "level")
    user.text = "7"
    user = xml.SubElement(user_list, "web")
    user.text = "1"
    user = xml.SubElement(user_list, "email")
    user.text = "1"
    user = xml.SubElement(user_list, "uwzd")
    user.text = "1"
    user = xml.SubElement(user_list, "levelname")
    user.text = "alert_forewarn_orange"

    user_list = xml.SubElement(v_user_list, "user")
    user_list.text = " "
    user = xml.SubElement(user_list, "id")
    user.text = "UWZDE38871"
    user = xml.SubElement(user_list, "level")
    user.text = "7"
    user = xml.SubElement(user_list, "web")
    user.text = "1"
    user = xml.SubElement(user_list, "email")
    user.text = "1"
    user = xml.SubElement(user_list, "uwzd")
    user.text = "1"
    user = xml.SubElement(user_list, "levelname")
    user.text = "alert_forewarn_orange"

    # user_list = xml.SubElement(v_user_list, "user")
    # user_list.text = " "
    # user = xml.SubElement(user_list, "id")
    # user.text = ""
    # user = xml.SubElement(user_list, "level")
    # user.text = "7"
    # user = xml.SubElement(user_list, "web")
    # user.text = "1"
    # user = xml.SubElement(user_list, "email")
    # user.text = "1"
    # user = xml.SubElement(user_list, "uwzd")
    # user.text = "1"
    # user = xml.SubElement(user_list, "levelname")
    # user.text = "alert_forewarn_orange"

    tree = xml.ElementTree(root)
    with open(fileName, "wb") as files:
        tree.write(files)



if __name__ == "__main__":
    detailed("test_data/detailed_warning/warn_16366607974214_20211111200404_1636794000_1636661108.xml")
