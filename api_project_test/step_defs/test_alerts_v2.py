# import requests
from pytest_bdd import scenarios, given, when, then, parsers
# from api_project_test.config.v2_methods import *
from api_project_test.config.configurations import get_config
from api_project_test.services.v2_methods import *

config = get_config()
target_fixture = "form_request"

# Shared Variables
token_cred = config.get('creds', 'auth_cred')
mg_auth_token_url = config.get('urls', 'mg_auth_token_url')
identity_auth_token_url = config.get('urls', 'identity_auth_token_url')

# Scenarios
scenarios('../features/alerts_v2.feature')


# Given Steps
@given("MG Authentication Token is set")
def mg_token():
    global mg_token
    mg_access_token = CommonApiUtils.auth_token(token_cred, mg_auth_token_url)
    mg_token = "Bearer " + mg_access_token


@given("Identity Authentication Token is set")
def identity_token():
    global identity_token
    identity_access_token = identity_auth_token_url()
    identity_token = "Bearer " + identity_access_token


@given(parsers.parse('that v2 API is queried with {showTotal} {country} and {excludeGeometry}'),
       target_fixture='form_request')
def form_request(country, showTotal, excludeGeometry):
    endpoint = config['urls']['dev_base_url'] + config['urls'][
        'v2_endpoint'] + "showTotal=" + showTotal + "&country=" + country + "&excludeGeometry=" + excludeGeometry

    return endpoint


@given(parsers.parse('no {country} is provided to query the v2 API'), target_fixture='form_request')
def form_request(country=None):
    if country == 'empty':
        endpoint = config['urls']['dev_base_url'] + config['urls']['v2_endpoint'] + "country="
    else:
        endpoint = config['urls']['dev_base_url'] + config['urls']['v2_endpoint']

    return endpoint


@given(parsers.parse('language {language} and country {country} are provided to query the v2 API'),
       target_fixture='form_request')
def form_request(country, language):
    showTotal = 'true'
    # excludeGeometry is temporarily added on the request to lessen response load
    excludeGeometry = 'true'

    endpoint = config['urls']['dev_base_url'] + config['urls'][
        'v2_endpoint'] + "showTotal=" + showTotal + "&country=" + country + "&excludeGeometry=" + excludeGeometry + "&language=" + language

    return endpoint


@given(parsers.parse('event {event} and country {country} are provided to query the v2 API'),
       target_fixture='form_request')
def form_request(country, event):
    showTotal = 'true'
    # excludeGeometry is temporarily added on the request to lessen response load
    excludeGeometry = 'true'

    endpoint = config['urls']['dev_base_url'] + config['urls'][
        'v2_endpoint'] + "showTotal=" + showTotal + "&country=" + country + "&excludeGeometry=" + excludeGeometry + "&event=" + event

    return endpoint


@given(parsers.parse('sender {sender} and country {country} are provided to query the v2 API'),
       target_fixture='form_request')
def form_request(country, sender):
    showTotal = 'true'
    # excludeGeometry is added on the request for now to lessen response load
    excludeGeometry = 'true'
    endpoint = config['urls']['dev_base_url'] + config['urls'][
        'v2_endpoint'] + "showTotal=" + showTotal + "&country=" + country + "&excludeGeometry=" + excludeGeometry + "&sender=" + sender

    return endpoint


@given(parsers.parse('startTime {startTime} endTime {endTime} and {country} are provided to query the v2 API'),
       target_fixture='form_request')
def form_request(country, startTime, endTime):
    showTotal = 'true'
    start_time, end_time = set_time_param()

    if startTime == "valid" and endTime == "empty":
        endpoint = config['urls']['dev_base_url'] + config['urls'][
            'v2_endpoint'] + "showTotal=" + showTotal + "&country=" + country + "&startTime=" + start_time

    elif startTime == "empty" and endTime == "valid":
        endpoint = config['urls']['dev_base_url'] + config['urls'][
            'v2_endpoint'] + "showTotal=" + showTotal + "&country=" + country + "&endTime=" + end_time

    else:
        endpoint = config['urls']['dev_base_url'] + config['urls'][
            'v2_endpoint'] + "showTotal=" + showTotal + "&country=" + country + "&startTime=" + start_time + "&endTime=" + end_time

    return endpoint


@given(parsers.parse('invalid startTime and endTime condition is provided with {country}'),
       target_fixture='form_request')
def form_request(country):
    showTotal = 'true'
    startTime, endTime = set_invalid_time_param()

    endpoint = config['urls']['dev_base_url'] + config['urls'][
        'v2_endpoint'] + "showTotal=" + showTotal + "&country=" + country + "&startTime=" + startTime + "&endTime=" + endTime

    return endpoint


@given(parsers.parse('empty startTime and endTime are provided'), target_fixture='form_request')
def form_request():
    showTotal = 'true'
    excludeGeometry = 'true'
    country = 'US'

    endpoint = config['urls']['dev_base_url'] + config['urls'][
        'v2_endpoint'] + "showTotal=" + showTotal + "&country=" + country + "&excludeGeometry=" + excludeGeometry + "&startTime=" + "" + "&endTime=" + ""

    return endpoint


@given(parsers.parse('v2 endpoint is queried with {showTotal} {excludeGeometry} {country} and {sender}'),
       target_fixture='form_request')
def form_request(showTotal, excludeGeometry, country, sender):
    startTime, endTime = set_time_param()

    endpoint = config['urls']['dev_base_url'] + config['urls'][
        'v2_endpoint'] + "showTotal=" + showTotal + "&country=" + country + "&excludeGeometry=" + excludeGeometry + "&sender=" + sender + \
               "&startTime=" + startTime + "&endTime=" + endTime

    return endpoint


@given(parsers.parse('startTime endTime and {country} are provided to query the v2 API'), target_fixture='form_request')
def form_request(country):
    showTotal = 'true'
    startTime, endTime = set_time_param()

    endpoint = config['urls']['dev_base_url'] + config['urls'][
        'v2_endpoint'] + "showTotal=" + showTotal + "&country=" + country + "&sender=NWS" + "&startTime=" + startTime + "&endTime=" + endTime

    return endpoint


@given(parsers.parse("{country} {language} {sender} {excludeGeometry} and {event} are provided to query the v2 API"),
       target_fixture='form_request')
def form_request(country, language, sender, excludeGeometry, event):
    showTotal = 'true'

    endpoint = config['urls']['dev_base_url'] + config['urls'][
        'v2_endpoint'] + "showTotal=" + showTotal + "&country=" + country + "&sender=" + sender + "&excludeGeometry=" + excludeGeometry + \
               "&event=" + event

    return endpoint


# When Steps
@when(parsers.parse('v2 alerts {method} API request is sent using {token} token'), target_fixture='my_send_request')
def my_send_request(method, form_request, token):
    if token.lower() == 'identity':
        token = identity_token
    elif token.lower() == 'mg':
        token = mg_token
    else:
        token = mg_token

    headers = {'Authorization': token}
    request = form_request
    response = CommonApiUtils.common_request(request, method, headers=headers, body=None, json=None)
    print("Response Url: " + response.url)
    # print("Response Body: " + response.content)

    return response


# Then Steps
@then(parsers.parse('the response status code is "{code:d}"'))
def response_code(my_send_request, code):
    assert my_send_request.status_code == code


@then(parsers.parse('alerts are returned'))
def response_contents(my_send_request):
    response_json = my_send_request.content
    response = json.loads(response_json)
    count = len(response['features'])

    total_alerts = response['totalAlerts']
    assert count == total_alerts, f"Failed:  {total_alerts} is not equal to count length."


@then(parsers.parse('alerts are returned with {event} provided'))
def response_contents(my_send_request, event):
    response_json = my_send_request.content
    response = json.loads(response_json)
    count = len(response['features'])
    counter = 0

    while counter < count:
        aba_event = response['features'][counter]['properties']['info'][0]['event']
        assert event in aba_event
        counter += 1


@then(parsers.parse('correct required {country_error_response} is returned'))
def response_contents(my_send_request, country_error_response):
    assert country_error_response == my_send_request.json()['detail']


@then(parsers.parse('correct alerts are returned based on {startTime} and {endTime} parameters'))
def response_content(my_send_request, startTime, endTime):
    response_json = my_send_request.content
    response = json.loads(response_json)

    verify_time_param(startTime, endTime, response)


@then(parsers.parse('correct {startTime} and {endTime} error response is returned'))
def response_content(my_send_request, startTime, endTime):
    if startTime > endTime:
        error_message = "endTime must not be earlier than startTime"
        assert error_message == my_send_request.json()['detail']


@then(parsers.parse('correct empty startTime and endTime error response is returned'))
def response_content(my_send_request):
    error_message = "invalid datetime format"
    assert error_message == my_send_request.json()['detail']['msg']


@then(parsers.parse('correct sender are returned'))
def response_content(my_send_request):
    response_json = my_send_request.content
    response = json.loads(response_json)
    count = len(response['features'])
    counter = 0

    while counter < count:
        sender = response['features'][counter]['properties']['sender']
        assert sender == "NWS"

        counter += 1


@then(parsers.parse('correct {sender} v2 data are returned'))
def response_content(my_send_request, sender):
    response_json = my_send_request.content
    response = json.loads(response_json)
    count = len(response['features'])
    counter = 0

    while counter < count:
        assert sender == response['features'][counter]['properties']['sender']

        # get aba alert original id
        get_original_id(response, counter)

        if sender == "NWS":
            # hit nws alerts url
            access_nws_alert_url(response, counter)

            try:
                # assert ABA data and nws details
                assert_nws_data(response, counter)

                # assert ABA and NWS timestamps
                # assert_nws_aba_timestamp(response, counter)
            except Exception as e:
                print("Exception error encountered", e)

        elif sender == "DWD":

            try:
                # hit dwd alerts url
                dwd_response = access_dwd_alert_url(response, counter)

                # convert dwd xml response content to json
                dwd_json = dwd_xml_to_dwd_json(dwd_response)

                # assert ABA data and nws details
                assert_dwd_data(response, counter, dwd_json, sender)

                # assert ABA and NWS timestamps
                # assert_nws_aba_timestamp(response, counter)
            except ConnectionError as e:
                print("Exception error encountered", e)

                assert e is True

        counter += 1

    print(Fore.GREEN + f"Passed: Verify v2 endpoint returns correct data as compared to the {sender} alert")


@then(parsers.parse('coordinates from NWS are mapped in ABA alert'))
def response_content(my_send_request):
    response_json = my_send_request.content
    response = json.loads(response_json)
    count = len(response['features'])
    counter = 0

    while counter < count:
        aba_polygon = response['features'][counter]['properties']['info'][0]['area'][0]
        if 'polygon' in aba_polygon:
            aba_id = response['features'][counter]['id']
            aba_coordinates = response['features'][counter]['properties']['info'][0]['area'][0]['polygon']['geometry'][
                'coordinates']
            aba_outer_coordinates = response['features'][counter]['geometry']['coordinates']
            get_original_id(response, counter)
            access_nws_alert_url(response, counter)
            nws_coordinates = check_nws_coordinates()
            # print(aba_id)
            try:
                assert len(aba_coordinates) == len(nws_coordinates)
                assert aba_coordinates == aba_outer_coordinates
            except Exception as e:
                print("Exception error encountered", e)
        else:
            return None
        counter += 1


@then(parsers.parse('geometries of NWS affectedZones are mapped in ABA alert'))
def response_content(my_send_request):
    response_json = my_send_request.content
    response = json.loads(response_json)
    count = len(response['features'])
    counter = 0

    while counter < count:
        aba_polygon = response['features'][counter]['properties']['info'][0]['area'][0]
        if 'polygon' in aba_polygon:
            return None
        else:
            # aba_id = response['features'][counter]['id']
            aba_geometrytype = response['features'][counter]['geometry']['type']
            if aba_geometrytype == 'GeometryCollection':
                aba_outer_geometries = response['features'][counter]['geometry']['geometries']
                assert len(aba_outer_geometries) != 0
            else:
                aba_outer_coordinates = response['features'][counter]['geometry']['coordinates']
                # print(aba_id)
                assert len(aba_outer_coordinates) != 0

        counter += 1


@then(parsers.parse("all parameters are returned in the response"))
def response_content(my_send_request):
    response_json = my_send_request.content
    response = json.loads(response_json)
    count = len(response['features'])
    counter = 0

    while counter < count:

        try:
            verify_optional_dwd_parameters(response, counter)
        except ConnectionError as e:
            print("Exception error encountered", e)

        counter += 1

    print(Fore.GREEN + f"Passed: Verify required and optional parameters of v2 endpoint for DWD")
